use regex::Regex;

pub fn use_multi_core(input_str: String, corecount: &str) -> String {


    let makere = Regex::new(r"(?m)^make(\s)").unwrap();
    let ninjare = Regex::new(r"(?m)^ninja(\s)").unwrap();

    println!("Applying your {} cores..", corecount);

    let fixninja = format!("ninja -j{}$1", corecount);
    let fixmake = format!("make -j{}$1", corecount);

    let use_cores = &input_str.replace("%corecount%", corecount);
    let didninja = ninjare.replace_all(use_cores, fixninja).to_string();

    let finished = makere.replace_all(&didninja, fixmake).to_string();

    finished

    //return file;
}
