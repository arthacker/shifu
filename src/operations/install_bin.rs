use crate::archive::untar;
use crate::download::download;
use crate::hash::get_hash_for_dir;
use crate::manage_bin::get_bin_info;
use crate::clean::clean;
use crate::questions::yes_or_no;
use crate::compression::copy_file_to_not_mox;
use colored::Colorize;
use fs_extra::dir;
use fs_extra::move_items;
use question::Answer;
use std::fs::remove_file;
use std::fs::File;
use std::io::prelude::*;
use std::process::exit;
use walkdir::WalkDir;

pub fn bin_install(name: &str, version: &str, path: &str) {
    let moxfile = format!(
        "/etc/shifu/cache/bin/{}/{}-{}.curtain.mox",
        name, name, version
    );
    let curtainfile = format!(
        "/etc/shifu/cache/bin/{}/{}-{}.curtain",
        name, name, version
    );

    download(
        format!(
            "https://ftp.curtainos.org/repo/amd64/{}-{}.curtain.mox",
            name, version
        )
        .as_str(),
        moxfile.as_str(),
    );
    copy_file_to_not_mox(moxfile.clone());
    untar(
        &curtainfile,
        format!("/etc/shifu/cache/bin/{}/{}-{}", name, name, version),
    );
    std::fs::remove_file(moxfile).unwrap();
    std::fs::remove_file(curtainfile).unwrap();

    let bindir = format!("/etc/shifu/cache/bin/{}/{}-{}", name, name, version);
    let hash = get_hash_for_dir(&bindir);
    let to_remove = format!("{}/", bindir);

    let hash1 = hash.replace(&to_remove, "");
    let wronghash = hash1.lines().last().unwrap();
    let new_hash = hash1.replace(wronghash, "");

    //load oldhash from hash.txt

    let mut content = File::open(format!("{}/hash.txt", bindir)).unwrap();
    let mut old_hash = String::new();

    //turn both hashes into iterators to check each value

    content
        .read_to_string(&mut old_hash)
        .expect("Unable to read the file");
    //remove empty line from var
    old_hash.pop();
    //iterate over var
    let olditer = old_hash.lines();

    //counter var for errors
    let mut is_there_an_error = 0;
    println!("{}", new_hash);
    for line in olditer {
        //checks for hash and filename in both hash texts
        if new_hash.find(line) > Some(0) {
            print!("{}", line);
            println!("[{}]", "OK".green());
            //fails are expected with info.toml and hash.txt
        } else if line.find("info.toml") > Some(0) || line.find("hash.txt") > Some(0) {
            print!("{}", line);
            println!("[{}]", "SKIPPED".blue());
        } else {
            print!("{}", line);
            println!("[{}]", "Error".red());
            //append one to error counter
            is_there_an_error += 1;
        }
    }
    //checks if there is a signature error
    if is_there_an_error > 0 {
        println!(
            "{}{} hash(es) could not be verified.",
            "WARNING: ".red(),
            is_there_an_error
        );
        //asks user weather to continue
        if !yes_or_no("Are you Sure you want to install this?", Answer::NO) {
            eprintln!("hashes could not be verified");
            exit(1);
        }
    }
    let info = get_bin_info(format!("{}/info.toml", bindir));
    println!(
        "You need {:?} to use {}",
        info.dependencies.needed, info.info.name
    );

    //initialize our copy CopyOptions
    let mut options = dir::CopyOptions::new(); //Initialize default values for CopyOptions
                                               //allow copy to overwrite
    options.overwrite = true;

    let mut from_paths = Vec::new();
    for entry in WalkDir::new(&bindir).min_depth(1).max_depth(1) {
        //parse through the fiorst files or folders in the dir
        let entry = entry.unwrap();
        from_paths.push(format!("{}", entry.path().display())); //append these to our "to copy" list
    }
    //move the items
    move_items(&from_paths, path, &options).unwrap();
    //remove hash.txt and info.toml_str
    remove_file("/info.toml").unwrap_or_else(|_| {
        println!("This binary did not come with a info.toml file");
    });
    remove_file("/hash.txt").unwrap_or_else(|_| {
        println!("This binary did not come with a hash.txt file");
    });
    println!("Succesfully installed {}🥳 Have a great day🥂", name);
    clean(name, "bin").unwrap();

}
