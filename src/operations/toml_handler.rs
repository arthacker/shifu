use serde_derive::{Deserialize, Serialize};

//_ __ ___  __ _  __| |
//| '__/ _ \/ _` |/ _` |
//| | |  __/ (_| | (_| |
//|_|  \___|\__,_|\__,_|

//Build file
//
//
#[derive(Deserialize, Default)]
pub struct HandleBuildfile {
    pub package: Package,
    pub dependencies: Dependencies,
    pub install: Install,
}

#[derive(Deserialize, Default)]
pub struct Package {
    pub name: String,
    pub ver: String,
    pub des: String,
    pub stripping: bool,
}

#[derive(Deserialize, Default)]
pub struct Dependencies {
    pub build: Option<Vec<String>>,
    pub depends: Option<Vec<String>>,
    pub source: String,
    pub opt: Option<Vec<String>>,
    pub conflicts: Option<Vec<String>>,
}

#[derive(Deserialize, Default)]
pub struct Install {
    pub installation: String,
    pub sysvinit: Option<String>,
}


//binary info
//
//
#[derive(Deserialize)]
pub struct Binfo {
    pub info: ReadInfo,
    pub dependencies: ReadBinDependencies,
}

#[derive(Deserialize)]
pub struct ReadInfo {
    pub name: String,
    pub version: String,
    pub description: String,
}
#[derive(Deserialize)]
pub struct ReadBinDependencies {
    pub needed: Option<Vec<String>>,
    pub optional: Option<Vec<String>>,
}

//__      ___ __(_) |_ ___
//\ \ /\ / / '__| | __/ _ \
//\ V  V /| |  | | ||  __/
//\_/\_/ |_|  |_|\__\___|

//write binary info
//
//
#[derive(Serialize)]
pub struct CreateInfo {
    pub info: Info,
    pub dependencies: BinDependencies,
}

#[derive(Serialize)]
pub struct Info {
    pub name: String,
    pub version: String,
    pub description: String,
}
#[derive(Serialize)]
pub struct BinDependencies {
    pub needed: Option<Vec<String>>,
    pub optional: Option<Vec<String>>,
}
