use crate::download::get_buildfile_or_error;
use crate::manage_git::clone;
use toml::Value;
use std::fs::File;
use std::io::prelude::*;
use crate::run::*;
use crate::toml_handler::HandleBuildfile;

pub fn git_install(name: &str) {
     //here we parse the config file
     let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
     let mut content = String::new();
      file.read_to_string(&mut content)
     .expect("Unable to read config file");
     let settings = content.parse::<Value>().unwrap();
     //end parsing config file
    let startroot = format!("/etc/shifu/cache/git/{}", name);
    let script = get_buildfile_or_error(format!("{}-git", name).as_str());
    let buildfile = mutate_buildfile(&script, settings);

    let config: HandleBuildfile = toml::from_str(buildfile.as_str()).expect("toml issue");
    clone(config.dependencies.source, &startroot);

    let buildfile = [
        config.install.installation,
        config.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");

    run(buildfile, format!("/etc/shifu/cache/git/{}", name));
}
