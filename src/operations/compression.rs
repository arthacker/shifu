use flate2::write::DeflateDecoder;
use flate2::write::DeflateEncoder;
use flate2::Compression;
use std::fs::File;
use std::io::BufReader;
use std::io::copy;
use std::time::Instant;

pub fn copy_file_to_mox(path: &String) {
    println!("{}", path);
    let mut input = BufReader::new(File::open(&path).unwrap());
    let output = File::create(format!("{}.mox", &path)).unwrap();

    let mut compressor = DeflateEncoder::new(output, Compression::default());
    let start = Instant::now();

    copy(&mut input, &mut compressor).unwrap();
    let output = compressor.finish().unwrap();

    println!("Source len:  {:?}",
        input.get_ref().metadata().unwrap().len()
    );
    println!("Target len:  {:?}", output.metadata().unwrap().len());
    println!("Elapsed:     {:?}", start.elapsed());
}

pub fn copy_file_to_not_mox(path: String) {
    
    let output_name = path.replace(".mox", "");
    println!("{}", path);
    let mut input = BufReader::new(File::open(&path).unwrap());
    let output = File::create(output_name).unwrap();

    let mut decompressor = DeflateDecoder::new(output);
    let start = Instant::now();

    copy(&mut input, &mut decompressor).unwrap();
    let output = decompressor.finish().unwrap();

    println!("Source len:  {:?}",
        input.get_ref().metadata().unwrap().len()
    );
    println!("Target len:  {:?}", output.metadata().unwrap().len());
    println!("Elapsed:     {:?}", start.elapsed());

}