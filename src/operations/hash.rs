use blake3;
use std::path::Path;

use walkdir::WalkDir;
use std::fs;

pub fn get_file_hash(file: String) -> String {
    let input = Path::new(&file);
    digest_file(input)
}

pub fn get_hash_for_dir(directory: &String) -> String {
    //let files = fs::read_dir(directory).unwrap();
    let mut collected = String::new();

    let walk_iter = WalkDir::new(directory).follow_links(false).into_iter();

    for entry_result in walk_iter {
        //println!("{}", entry.path().display());

        match entry_result {
            Ok(entry) => {
                if entry.metadata().unwrap().is_file() {
                    let hashed = get_file_hash(format!("{}", entry.path().display()));
                    collected
                        .push_str(format!("{}  {}\n", hashed, entry.path().display()).as_str());
                }
            }
            Err(e) => {
                eprintln!("{:?}", e);
                continue;
            }
        };
    }
    collected
}

pub fn digest_file<P: AsRef<Path>>(path: P) -> String {
    let bytes = fs::read(path).unwrap();
    let hash = blake3::hash(&bytes);
    hash.to_string()
}