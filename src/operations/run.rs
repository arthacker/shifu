use crate::use_prefix::use_prefix;
use crate::{replace_placeholders::replace_placeholders, use_multiple_cores::use_multi_core};
use std::env;
use toml::Value;
use std::fs;
use crate::download::download;
use std::path::Path;
use std::process::Command;

pub fn run(string_to_run: String, path: String) {
    let startroot = path;
    let newstring = string_to_run.replace("%destdir%", "/");
    let mut root = Path::new(&startroot);
    fs::create_dir_all(&root).unwrap();


    let mut _newroot = String::new();

    if root.read_dir().unwrap().next().is_none() {
        println!("dir is empty");
        root = root.parent().unwrap();
    }

    for line in newstring.lines() {
        println!("{}", root.display());



        env::set_current_dir(&root).unwrap();

        if line.trim().is_empty() || line.starts_with('#') || line.starts_with("//") {
            continue;
    
            
        } else if line.starts_with("download") {
            let arguments: Vec<&str> = line.split_whitespace().collect();
            if arguments.len() == 4 {
            download(arguments[1], arguments[3])
            }
            else {
                println!("please download files in a valid syntax");
                std::process::exit(1);
            }

    }
        else if line.contains('=') || line.starts_with("./") || line.contains('[') || line.contains("unset") || line.contains('$') || line.contains('}') || line.contains('|') || line.starts_with("sed") || line.contains('*'){
            Command::new("bash")
                .arg("-c")
                .arg(line)
                .spawn()
                .unwrap()
                .wait()
                .unwrap();

            continue;
        } else if line.starts_with("cd ..") {
            root = Path::new(root.parent().unwrap());

            continue;
        } else if line.starts_with("cd") {

            let cd_command: Vec<&str> = line.split_whitespace().collect();
            _newroot = format!("{}/{}", root.display(), cd_command[1]);
            root = Path::new(&_newroot);

            continue;
        } else {
            let args: Vec<&str> = line.split_whitespace().collect();
            println!("{:?}", args);
            Command::new(&args[0])
                .args(&args[1..])
                .spawn()
                .unwrap()
                .wait()
                .unwrap();
            continue;
        }
    }
}
pub fn mutate_buildfile(buildfile: &String, config: Value) -> String {
    println!("{}", buildfile);

    let firstedit = replace_placeholders(buildfile);
    let secondedit = use_multi_core(firstedit, config["hardware"]["core_count"].as_str().unwrap());
    use_prefix(secondedit, config["configuration"]["prefix"].as_str().unwrap())
}
