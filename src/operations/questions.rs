use colored::Colorize;
use question::{Answer, Question};
use std::io;

pub fn yes_or_no(question: &str, default: Answer) -> bool {
    let answer = Question::new(question)
        .default(default)
        .show_defaults()
        .confirm();

    answer == Answer::YES
}
pub fn choose_from_multiple(mut options: Vec<&str>, question: &str, help: &str) -> String {
    if options.len() > 1 {
        let mut counter = 1;
        options.push("help");
        for item in &options {
            print!("{}) ", counter);
            println!("{}", item.yellow());
            counter += 1;
        }

        loop {
            print!("{}: [", question);
            for i in 1..counter {
                print!(", {}", i);
            }
            println!("]");
            let mut numin = String::new();
            io::stdin()
                .read_line(&mut numin)
                .expect("Failed to read line");

            let numin: usize = match numin.trim().parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("please enter a number!");
                    continue;
                }
            };

            if numin > counter {
                println!("please enter a number in the range from 1 to {}", counter);
            } else {
                //we need to take one off because Vectors start with the index 0, not 1
                let usednum = numin - 1;
                if options[usednum] == "help" {
                    println!("{}\n\n", help);
                    continue;
                }
                return options[usednum].to_string();
            }
        }
    } else {
        options[0].to_string()
    }
}
