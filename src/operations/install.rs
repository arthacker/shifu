use crate::download::get_buildfile;
use crate::errors::invalid_toml_format;
use crate::install_bin::bin_install;
use crate::install_git::git_install;
use crate::install_source::source_install;
use crate::manage::check_if_toml_exists;
use crate::manage_bin::check_if_bin_exists;
use crate::questions::*;

use colored::Colorize;
use std::process::exit;
//use std::process::exit;

use crate::toml_handler::HandleBuildfile;

pub fn install(package_name: &str) {
    //start the function if just the package name was presented
    let install_script = get_buildfile(package_name);
    let config: HandleBuildfile = Default::default();
    //predeclare the config, because it could stay unused
    let mut availible_versions = Vec::new();

    if !install_script.trim().is_empty() {
        print!("{}", install_script);
        let config: HandleBuildfile =
            toml::from_str(install_script.as_str()).unwrap_or_else(|_| {
                invalid_toml_format();
                exit(1);
            });

        println!(
            "Package {} found with ver: {}",
            package_name.red(),
            config.package.ver.green()
        );
        availible_versions.push("compiled");
    }
    let bincheck = check_if_bin_exists(package_name);

    if bincheck == "nope, this binary does not exist" {
        println!("bin not found")
    } else {
        println!(
            "Binary {} found with ver: {}",
            package_name.red(),
            bincheck.green()
        );
        availible_versions.push("binary");
    }

    if !check_if_toml_exists(format!("{}-git", package_name).as_str()) {
        println!("git version not found")
    } else {
        println!(
            "package {} found with ver: {}",
            package_name.red(),
            "git".green()
        );
        availible_versions.push("git");
    }

    let wanted_version = choose_from_multiple(
        availible_versions,
        "which version do you want to install? choose helpfor info on the diiferences",
        "(binary) quickly installed, bigger
         (compiled) takes a while but more system specific
         (git) compiling newest git source, unstable but new",
    );

    match wanted_version.as_str() {
        "git" => git_install(package_name),
        "compiled" => source_install(config, install_script),
        "binary" => bin_install(package_name, &bincheck, "/"),
        _ => println!("Yo WTF did you to actually get this output? Absoloute chad"),
    }
}
