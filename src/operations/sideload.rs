use crate::archive::async_untar;
use crate::download::async_download;
use crate::errors::bin_not_exist;
use crate::hash::get_hash_for_dir;
use crate::errors::error_verifying_hash;
use crate::compression::copy_file_to_not_mox;
use colored::Colorize;
use futures::stream; // 0.3.
use std::io::Read;
use crate::questions::yes_or_no;
use crate::query::handle_query;
use tokio;
use std::fs::File;
use question::Answer;
use std::fs;
use futures::StreamExt;
use toml::Value;
use std::path::Path;

pub fn sideload(input: Vec<String>) {
       //here we parse the config file
       let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
       let mut content = String::new();
        file.read_to_string(&mut content)
       .expect("Unable to read config file");
       let settings = content.parse::<Value>().unwrap();
       //end parsing config file
   

    let veccount = input.len();
    let destination = get_absolute_path(&input[veccount - 1].clone());
    let input = handle_query((&input[..(veccount - 1)]).to_vec(), settings);
    println!("{}", destination);
    let full_names = get_ver_for_bin(&input);
    let urls = get_url_for_name(&full_names);
    println!("{:?}", full_names);
    async_download(urls, input, &destination);
    println!("func ran");
    std::env::set_current_dir(&destination).unwrap();
    let mut not_compressed_vec = Vec::new();
    for i in &full_names {
        copy_file_to_not_mox(format!("{}/{}.curtain.mox", destination, i));
        let some_temp_var = i.replace(".mox", "");
        not_compressed_vec.push(some_temp_var.to_string());
    }   
    async_untar(&not_compressed_vec, &destination);
    async_hash(&full_names, &destination);
    for file in fs::read_dir("./").unwrap() {
        
            let file = file.unwrap();
        if file.path().extension().unwrap_or_default() == "mox"||file.path().extension().unwrap_or_default() == "curtain" {
            fs::remove_file(file.path()).unwrap();
    }}
    install_sideloaded(full_names, destination);

}



fn install_sideloaded(packages: Vec<String>, destination: String) {
    for i in packages {
     fs::remove_file(i.to_owned() + "/" + "hash.txt").unwrap_or_default();
     fs::remove_file(i.to_owned() + "/" + "info.toml").unwrap_or_default();
     for file in fs::read_dir("./".to_owned() + i.to_owned().as_str()).unwrap() {
         let file = file.unwrap();
         let to_copy: String = file.path().display().to_string();
        std::process::Command::new("cp").arg("-r").arg(to_copy).arg(&destination).spawn().unwrap().wait().unwrap();

     }
     fs::remove_dir_all(i).unwrap();

    }

}
fn get_ver_for_bin(packages: &Vec<String>) -> Vec<String> {
    let response =
        reqwest::blocking::get("https://ftp.curtainos.org/repo/amd64/index.toml")
            .unwrap_or_else(|e| {
                eprintln!("Unknown error: {}", e);
                std::process::exit(1);
            });
    let index = response.text().unwrap();
    let toml_index = index
        .as_str()
        .parse::<Value>()
        .ok()
        .and_then(|r| match r {
            Value::Table(table) => Some(table),
            _ => None,
        })
        .unwrap();
    let mut names = Vec::new();
    for i in packages {
        if toml_index.get(i) == None {
            bin_not_exist(i.to_string());
        }
        names.push(format!("{}-{}", i, toml_index[i].as_str().unwrap()));
    }

    names
}
fn get_url_for_name(names: &Vec<String>) -> Vec<String> {
    let mut return_string: Vec<String> = Vec::new();
    for i in names {
        return_string.push(format!(
            "https://ftp.curtainos.org/repo/amd64/{}.curtain.mox",
            i
        ));
    }
    return_string
}

pub fn get_absolute_path(path: &str) -> String {
    let mut path = path.to_string();
    if path.starts_with("./") {
        path = path.replace("./", "");
    }
    if !path.starts_with('/') {
        path = std::env::current_dir().unwrap().to_str().unwrap().to_owned() + "/" + &path;
    }
    if path.ends_with('/') {
            path.pop();
        }

    if !Path::new(&path).exists() {
        eprintln!("{}: {}", "Directory does not exist".red(), path.as_str().green());
        std::process::exit(1);
    }


    path
}


#[tokio::main]
async fn async_hash(packages: &Vec<String>, destdir: &String) {
    let total_packages = packages.len();
    let runners = stream::iter(packages).map(|package| {
        async {
        println!("hiii" );
        let workdir = destdir.to_owned() + "/" + package.as_str();
        println!("{:?}", workdir);
        let hash = get_hash_for_dir(&workdir);
        let to_replace = workdir.to_owned() + "/";
        let new_hash = hash.replace(&to_replace, "");
        let mut  file = std::fs::File::open(workdir + "/hash.txt").unwrap();
        let mut old_hash = String::new();

        //turn both hashes into iterators to check each value

        file
            .read_to_string(&mut old_hash)
            .expect("Unable to read the file");
        //remove empty line from var
        old_hash.pop();
        //iterate over var
        let olditer = old_hash.lines();
        let mut error = false;
            println!("{}", new_hash);
        for line in olditer {
            //checks for hash and filename in both hash texts
        
            if new_hash.find(line) > Some(0) {
                print!("{}", line);
                println!("[{}]", "OK".green());
                //fails are expected with info.toml and hash.txt
            } else if line.find("info.toml") > Some(0) || line.find("hash.txt") > Some(0) || line.find("xattr.conf") > Some(0){
                print!("{}", line);
                println!("[{}]", "SKIPPED".blue());
            } else {
                print!("{}", line);
                println!("[{}]", "Error".red());
                error = true
                //append one to error counter
                //is_there_an_error += 1;
            }
        }

        error
    }}).buffer_unordered(total_packages);

    runners
        .for_each(|b| async move {
            println!("{}", b);
                    match b {
                      false => {
            println!("{}", "Verified Hash".green());
                        }
                    true => {
                    if !yes_or_no("Are you Sure you want to install this?", Answer::NO) {
                        error_verifying_hash()

                    }}
    }    })
        .await;

}
