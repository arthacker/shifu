use git2::Error;
use git2::Repository;
use std::fs;
use std::path::Path;

pub fn clone(url: String, destination: &String) {
    let clonepath = Path::new(&destination);
    fs::create_dir_all(destination).unwrap();
    println!("{} to {}", url, destination);
    if clonepath.read_dir().unwrap().next().is_none() {
        print!("this runs emty dir");
        match Repository::clone(&url, destination) {
            Ok(repo) => repo,
            Err(e) => panic!("failed to clone: {}", e),
        };
    } else {
        match Repository::open(destination) {
            Ok(repo) => {
                pull(repo).unwrap();
            }
            Err(_e) => {
                panic!(
                    "the directory {} seems to be occupied for your own files, please move them",
                    destination
                );
            }
        };
    }
}

fn pull(repo: Repository) -> Result<(), Error> {
    repo.find_remote("origin")?.fetch(&["master"], None, None)?;

    let fetch_head = repo.find_reference("FETCH_HEAD")?;
    //get current commit
    let fetch_commit = repo.reference_to_annotated_commit(&fetch_head)?;
    //analize repo
    let analysis = repo.merge_analysis(&[&fetch_commit])?;
    //check if repo is up to date
    if analysis.0.is_up_to_date() {
        println!("cloned repo is up to date");
        Ok(())
    } else if analysis.0.is_fast_forward() {
        //get refname
        let refname = "refs/heads/master";
        let mut reference = repo.find_reference(refname)?;
        //set target for pull
        reference.set_target(fetch_commit.id(), "Fast-Forward")?;
        repo.set_head(refname)?;
        //chckout
        repo.checkout_head(Some(git2::build::CheckoutBuilder::default().force()))
    } else {
        Err(Error::from_str("Fast-forward only!"))
    }
}
