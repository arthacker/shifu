
  <a href="https://ci.codeberg.org/CurtainOS/shifu" title="build">
    <img src="https://ci.codeberg.org/api/badges/CurtainOS/shifu/status.svg">
  </a>




## This is the official package manager for CurtainOS.

working: installing
issues: dependencies that dont listen to their name, such as rust arent recognized
not working: updating, deleting and any other features, no database




## Installation
*Runtime-requirements:* wget, openssl (libressl was not tested)
*Build-requirements:* cargo, rustc

*Instructions:*
to install the package simply enter this command:

```
cargo install --path . --root /
```

It is strongly advised to install shifu into / instead of $HOME/.cargo (cargo\`s default dir).
In case you are building this on a non CurtainOS build, you are required to make a directory called /etc/shifu and put the provided config.toml into it.








## Missing features
- Use a database to store installed packages and "required by" for each package
- Check if a -git and -bin exists and ask user which version they want to use
- Add more functions next to install such as delete, update, update as {git, bin}
